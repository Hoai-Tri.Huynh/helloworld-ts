# Étape de build
FROM node:slim AS builder
WORKDIR /app
# Copie des fichiers de dépendances
COPY package*.json ./
# Installation des dépendances
RUN npm install
# Copie du reste des fichiers du projet
COPY . .
# Construction du projet
RUN npm run build

# Étape de run
FROM node:alpine
WORKDIR /app
# Copie des artefacts de construction du builder
COPY --from=builder /app/dist ./dist
COPY package*.json ./
# Installation des dépendances de production uniquement
RUN npm install --only=production
# Commande pour exécuter l'application
CMD ["node", "dist/index.js"]
